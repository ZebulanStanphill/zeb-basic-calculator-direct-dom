'use strict'

// External dependencies
import Decimal from 'decimal.js'
import toFormat from 'toformat'

// Internal dependencies
import './assets/style.scss'

const Dec = toFormat(Decimal)

// This declaration is merged with the official one. It adds the toFormat method to the Decimal type, which at this point in the code has already been added to the Decimal prototype by the toFormat function call above.
// Obviously, this is a bit of a hacky way of doing things. But after hours of searching online, I was unable to find a better solution. It's kind of weird to be modifying classes dynamically anyway, so it's not surprising that it's difficult to make TypeScript happy when you do.
declare module 'decimal.js' {
	interface Decimal {
		toFormat(
			dp?: number,
			rm?: number,
			fmt?: {
				decimalSeparator?: string
				groupSeparator?: string
				groupSize?: number
				secondaryGroupSize?: number
				fractionGroupSeparator?: string
				fractionGroupSize?: number
			}
		): string
	}
}

enum BinaryOperator {
	Add,
	Divide,
	Multiply,
	Subtract,
}

const OPERATOR_SYMBOLS: Record<BinaryOperator, string> = {
	[BinaryOperator.Add]: '+',
	[BinaryOperator.Subtract]: '−',
	[BinaryOperator.Multiply]: '×',
	[BinaryOperator.Divide]: '∕',
}

enum CalculatorMode {
	Start,
	AfterDigit,
	AfterOperator,
}

type Digit = 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9

function doBinaryOperation(n1: Decimal, n2: Decimal, operator: BinaryOperator) {
	switch (operator) {
		case BinaryOperator.Add:
			return n1.plus(n2)
		case BinaryOperator.Subtract:
			return n1.minus(n2)
		case BinaryOperator.Multiply:
			return n1.times(n2)
		case BinaryOperator.Divide:
			return n1.dividedBy(n2)
	}
}

const displayEl = document.getElementById(
	'zbc-calculator__display__output'
) as HTMLOutputElement

let lastNumber = new Dec('0')
let lastBinaryOperator: BinaryOperator | null = null
let mode = CalculatorMode.Start

// User-facing string shown in calculator display. Adds commas to large numbers.
function getFormattedOutput(inputString: string) {
	if (mode === CalculatorMode.Start || mode === CalculatorMode.AfterDigit) {
		// Parsing "0." into a number will just give 0, so to make it clear to the user that their input was taken, append the locale's decimal separator to the end of the displayed number.
		if (inputString.endsWith('.')) {
			return new Dec(inputString).toFormat() + '.'
		} else {
			return new Dec(inputString).toFormat()
		}
	} else {
		// If the display is currently showing a binary operator, just show it as-is.
		return inputString
	}
}

// Special thanks to https://stackoverflow.com/questions/1759987/listening-for-variable-changes-in-javascript/37403125#37403125
const currentInputString = {
	internalValue: '0',
	get value() {
		return this.internalValue
	},
	set value(newValue: string) {
		this.internalValue = newValue

		displayEl.value = getFormattedOutput(newValue)
	},
}

function reset(): void {
	lastNumber = new Dec('0')
	lastBinaryOperator = null
	mode = CalculatorMode.Start
	currentInputString.value = '0'
}

function backspace(): void {
	if (mode === CalculatorMode.AfterDigit) {
		currentInputString.value = currentInputString.value.slice(0, -1)
	}
}

// When a digit button is pressed.
function inputDigit(digit: Digit): void {
	if (mode === CalculatorMode.AfterDigit) {
		currentInputString.value += String(digit)
	} else {
		// If at initial state or after operator.
		currentInputString.value = String(digit)
	}

	mode = CalculatorMode.AfterDigit
}

// When the decimal point button is pressed.
function inputDecimalPoint(): void {
	if (
		mode === CalculatorMode.AfterDigit &&
		!currentInputString.value.includes('.')
	) {
		currentInputString.value += '.'
	} else if (
		mode === CalculatorMode.Start ||
		mode === CalculatorMode.AfterOperator
	) {
		currentInputString.value = '0.'
	}

	mode = CalculatorMode.AfterDigit
}

function negate(): void {
	if (mode === CalculatorMode.Start || mode === CalculatorMode.AfterDigit) {
		currentInputString.value = String(-currentInputString.value)
	}
}

function inputBinaryOperator(operator: BinaryOperator): void {
	if (mode === CalculatorMode.AfterDigit || mode === CalculatorMode.Start) {
		if (lastBinaryOperator !== null) {
			lastNumber = doBinaryOperation(
				lastNumber,
				new Dec(currentInputString.value),
				lastBinaryOperator
			)
		} else {
			lastNumber = new Dec(currentInputString.value)
		}
		lastBinaryOperator = operator
		mode = CalculatorMode.AfterOperator
		currentInputString.value = OPERATOR_SYMBOLS[operator]
	}
}

const inputMultiplyOperator = () => inputBinaryOperator(BinaryOperator.Multiply)
const inputDivideOperator = () => inputBinaryOperator(BinaryOperator.Divide)
const inputAddOperator = () => inputBinaryOperator(BinaryOperator.Add)
const inputSubtractOperator = () => inputBinaryOperator(BinaryOperator.Subtract)

function inputEquals(): void {
	if (mode !== CalculatorMode.Start && lastBinaryOperator !== null) {
		const result = doBinaryOperation(
			lastNumber,
			new Dec(currentInputString.value),
			lastBinaryOperator
		)

		lastNumber = result
		lastBinaryOperator = null
		mode = CalculatorMode.Start
		currentInputString.value = String(result)
	}
}

function handleKeyPress({ key }: KeyboardEvent): void {
	switch (key) {
		case 'c':
			reset()
			break
		case 'b':
			backspace()
			break
		case 'n':
			negate()
			break
		case '+':
			inputAddOperator()
			break
		case '-':
			inputSubtractOperator()
			break
		case '*':
			inputMultiplyOperator()
			break
		case '/':
			inputDivideOperator()
			break
		case '0':
			inputDigit(0)
			break
		case '1':
			inputDigit(1)
			break
		case '2':
			inputDigit(2)
			break
		case '3':
			inputDigit(3)
			break
		case '4':
			inputDigit(4)
			break
		case '5':
			inputDigit(5)
			break
		case '6':
			inputDigit(6)
			break
		case '7':
			inputDigit(7)
			break
		case '8':
			inputDigit(8)
			break
		case '9':
			inputDigit(9)
			break
		case '.':
			inputDecimalPoint()
			break
		case '=':
			inputEquals()
	}
}

document.addEventListener('keypress', handleKeyPress)

document.getElementById('zbc-button-reset')?.addEventListener('click', reset)

document
	.getElementById('zbc-button-backspace')
	?.addEventListener('click', backspace)

const digitButtons = document.querySelectorAll('.zbc-button.is-digit-button')

for (const button of digitButtons) {
	button.addEventListener('click', ({ target }) => {
		const digitString = (target as HTMLButtonElement).value

		inputDigit(parseInt(digitString) as Digit)
	})
}

document
	.getElementById('zbc-button-decimal')
	?.addEventListener('click', inputDecimalPoint)

document.getElementById('zbc-button-negate')?.addEventListener('click', negate)

document
	.getElementById('zbc-button-add')
	?.addEventListener('click', inputAddOperator)

document
	.getElementById('zbc-button-subtract')
	?.addEventListener('click', inputSubtractOperator)

document
	.getElementById('zbc-button-multiply')
	?.addEventListener('click', inputMultiplyOperator)

document
	.getElementById('zbc-button-divide')
	?.addEventListener('click', inputDivideOperator)

document
	.getElementById('zbc-button-equals')
	?.addEventListener('click', inputEquals)
