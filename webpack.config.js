/* eslint-env node */
const path = require('path')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

const srcFolder = path.resolve(__dirname, 'src')

module.exports = {
	mode: 'production',
	entry: './src/main.ts',
	output: {
		filename: 'bundle.[hash].js',
		path: path.resolve(__dirname, 'dist'),
	},
	devtool: 'source-map',
	module: {
		rules: [
			{
				test: /\.jsx?$/u,
				include: [srcFolder],
				use: ['babel-loader', 'eslint-loader'],
			},
			{
				test: /\.tsx?$/u,
				include: [srcFolder],
				use: ['babel-loader', 'ts-loader'],
			},
			{
				test: /\.scss$/u,
				include: [srcFolder],
				use: [
					{
						loader: MiniCssExtractPlugin.loader,
						options: {
							publicPath: '../',
						},
					},
					'css-loader',
					'sass-loader',
				],
			},
			{
				test: /\.(ttf|woff2)$/u,
				include: [srcFolder],
				use: [
					{
						loader: 'file-loader',
						options: {
							outputPath: './assets/fonts',
						},
					},
				],
			},
		],
	},
	resolve: {
		/* Without this, importing .tsx or .ts files without specifying the extension won't work, and TypeScript won't let you use explicit extensions in imports. */
		extensions: ['.tsx', '.ts', '.jsx', '.js'],
	},
	plugins: [
		new CleanWebpackPlugin(),
		new HtmlWebpackPlugin({
			inject: 'head',
			scriptLoading: 'defer',
			template: './src/index.html',
			xhtml: true,
		}),
		new MiniCssExtractPlugin({
			filename: './assets/[name].css',
		}),
	],
}
