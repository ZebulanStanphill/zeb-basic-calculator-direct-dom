# Zeb’s Basic Calculator (direct DOM version)

## Description

A version of my calculator project that uses [TypeScript](https://www.typescriptlang.org/) and [SCSS](https://sass-lang.com/), but no front-end UI libraries/frameworks.

Check out the [Preact version](https://gitlab.com/ZebulanStanphill/zeb-basic-calculator-preact) and the [Vue version](https://gitlab.com/ZebulanStanphill/zeb-basic-calculator-vue).

## Usage

1. Open a terminal at the project root.
2. Run `npm install` to install the package dependencies.
3. Run `npm run build` to build the project.
4. Open `dist/index.html` to view/use the project.

## Licensing info

© 2018-2020 [Zebulan Stanphill](https://zebulan.com/)

This program is free (as in freedom) software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see https://www.gnu.org/licenses/.

This project uses the [Nunito font](https://github.com/vernnobile/NunitoFont).

© 2014 Vernon Adams (vern@newtypography.co.uk).

Released under the SIL Open Font License, Version 1.1. See `nunito-license.txt`.
